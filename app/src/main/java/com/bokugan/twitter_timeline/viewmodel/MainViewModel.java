package com.bokugan.twitter_timeline.viewmodel;

import com.bokugan.twitter_timeline.repository.TweetDataSourceFactory;
import com.bokugan.twitter_timeline.repository.TwitterRepository;
import com.bokugan.twitter_timeline.twitter.Tweet;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

public class MainViewModel extends ViewModel {
    private static final int CHUNK_SIZE = 10;

    private final TwitterRepository repo;
    private final LiveData<PagedList<Tweet>> tweets;
    private final MutableLiveData<DataState> dataState;

    public MainViewModel() {
        // TODO. Use Dagger.
        repo = new TwitterRepository();
        tweets = Transformations.switchMap(repo.getTweetFactory(), this::updateTweets);
        // TODO. Move to repo?
        dataState = new MutableLiveData<>();
        dataState.setValue(DataState.NOTHING);
    }

    public LiveData<DataState> getDataState() {
        return dataState;
    }

    public LiveData<PagedList<Tweet>> getTweets() {
        return tweets;
    }

    public LiveData<String> getUserId() {
        return repo.getUserId();
    }

    public void setUserId(String userId) {
        repo.setUserId(userId);
    }

    public void refreshData() {
        dataState.setValue(DataState.LOADING);
        tweets.getValue().getDataSource().invalidate();
    }

    private LiveData<PagedList<Tweet>> updateTweets(TweetDataSourceFactory factory) {
        dataState.setValue(DataState.LOADING);

        return new LivePagedListBuilder<>(factory, CHUNK_SIZE)
                .setBoundaryCallback(new PagedList.BoundaryCallback<Tweet>() {
                    @Override
                    public void onZeroItemsLoaded() {
                        super.onZeroItemsLoaded();
                        dataState.setValue(DataState.NOTHING);
                    }

                    @Override
                    public void onItemAtFrontLoaded(@NonNull Tweet itemAtFront) {
                        super.onItemAtFrontLoaded(itemAtFront);
                        dataState.setValue(DataState.LOADED);
                    }

                    @Override
                    public void onItemAtEndLoaded(@NonNull Tweet itemAtEnd) {
                        super.onItemAtEndLoaded(itemAtEnd);
                        dataState.setValue(DataState.LOADED);
                    }
                })
                .build();
    }

    public enum DataState {LOADING, LOADED, NOTHING}
}
