package com.bokugan.twitter_timeline.repository;

import com.bokugan.twitter_timeline.twitter.ITwitterAPI;
import com.bokugan.twitter_timeline.twitter.Tweet;

import androidx.paging.DataSource;

public class TweetDataSourceFactory extends DataSource.Factory<Long, Tweet> {
    private ITwitterAPI twitterAPI;
    private String userId;

    public TweetDataSourceFactory(ITwitterAPI twitterAPI, String userId) {
        this.twitterAPI = twitterAPI;
        this.userId = userId;
    }

    @Override
    public DataSource<Long, Tweet> create() {
        TweetDataSource source = new TweetDataSource(twitterAPI, userId);
        return source;
    }
}
