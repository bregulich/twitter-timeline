package com.bokugan.twitter_timeline.repository;

import com.bokugan.twitter_timeline.twitter.ITwitterAPI;
import com.bokugan.twitter_timeline.twitter.Tweet;
import com.bokugan.twitter_timeline.twitter.utils.FutureCallWrapper;
import com.bokugan.twitter_timeline.twitter.utils.TweetsParameterRequestBuilder;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.paging.ItemKeyedDataSource;

public class TweetDataSource extends ItemKeyedDataSource<Long, Tweet> {
    private ITwitterAPI twitterAPI;
    private String userId;

    // https://developer.twitter.com/en/docs/tweets/timelines/guides/working-with-timelines
    private long sinceId = 0;
    private long maxId = Long.MAX_VALUE;

    public TweetDataSource(ITwitterAPI twitterAPI, String userId) {
        super();
        this.twitterAPI = twitterAPI;
        this.userId = userId;
    }

    @NonNull
    @Override
    public Long getKey(@NonNull Tweet item) {
        return item.getId();
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Long> params, @NonNull LoadInitialCallback<Tweet> callback) {
        clearBorderlineIds();

        twitterAPI.tweetsFuture(
                new TweetsParameterRequestBuilder()
                        .setUserId(userId)
                        .setCount(params.requestedLoadSize))
                .thenCompose(FutureCallWrapper::getFuture)
                .handle((tweets, throwable) -> {
                    handleTweetResponse(tweets, callback);
                    return null;
                });
    }

    @Override
    public void loadAfter(@NonNull LoadParams<Long> params, @NonNull LoadCallback<Tweet> callback) {
        loadBeforeAfterTask(
                new TweetsParameterRequestBuilder()
                        .setUserId(userId)
                        .setCount(params.requestedLoadSize)
                        .setMaxId(maxId),
                callback);
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Long> params, @NonNull LoadCallback<Tweet> callback) {
        loadBeforeAfterTask(
                new TweetsParameterRequestBuilder()
                        .setUserId(userId)
                        .setCount(params.requestedLoadSize)
                        .setSinceId(sinceId),
                callback);
    }

    // TODO. Rename.
    private void loadBeforeAfterTask(TweetsParameterRequestBuilder builder, LoadCallback<Tweet> callback) {
        twitterAPI.tweetsFuture(builder)
                .thenCompose(FutureCallWrapper::getFuture)
                .handle((tweets, throwable) -> {
                    handleTweetResponse(tweets, callback);
                    return null;
                });
    }


    // TODO. Optimize.
    // https://developer.twitter.com/en/docs/tweets/timelines/guides/working-with-timelines
    private void updateBorderlineIds(List<Tweet> tweets) {
        for (Tweet t : tweets) {
            long id = t.getId();
            sinceId = Math.max(sinceId, id);
            maxId = Math.min(maxId, id);
        }

        if (--maxId < 0)
            maxId = 0;
    }

    private void clearBorderlineIds() {
        maxId = Long.MAX_VALUE;
        sinceId = 0;
    }

    private void handleTweetResponse(List<Tweet> tweets, LoadInitialCallback<Tweet> callback) {
        tweets = tweets == null ? new ArrayList<>() : tweets;
        updateBorderlineIds(tweets);
        callback.onResult(tweets);
    }

    private void handleTweetResponse(List<Tweet> tweets, LoadCallback<Tweet> callback) {
        tweets = tweets == null ? new ArrayList<>() : tweets;
        updateBorderlineIds(tweets);
        callback.onResult(tweets);
    }
}
