package com.bokugan.twitter_timeline.repository;

import com.bokugan.twitter_timeline.twitter.ITwitterAPI;
import com.bokugan.twitter_timeline.twitter.TwitterAPI;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

public class TwitterRepository {
    private ITwitterAPI rest;
    private MutableLiveData<String> userId;
    private LiveData<TweetDataSourceFactory> tweetFactory;

    public TwitterRepository() {
        rest = TwitterAPI.getInstance();
        userId = new MutableLiveData<>();
        tweetFactory = Transformations.switchMap(userId, this::updateTweetFactory);
    }

    public LiveData<TweetDataSourceFactory> getTweetFactory() {
        return tweetFactory;
    }

    public LiveData<String> getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId.setValue(userId);
    }

    private LiveData<TweetDataSourceFactory> updateTweetFactory(String userId) {
        MutableLiveData<TweetDataSourceFactory> liveData = new MutableLiveData<>();
        liveData.setValue(new TweetDataSourceFactory(rest, userId));
        return liveData;
    }
}
