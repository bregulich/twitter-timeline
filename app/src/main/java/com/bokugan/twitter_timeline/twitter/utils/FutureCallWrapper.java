package com.bokugan.twitter_timeline.twitter.utils;

import java9.util.concurrent.CompletableFuture;
import retrofit2.Call;

// Wrapper for a combined CompletableFuture + retrofit2.Call cancellation.
// TODO. Refactor.
public class FutureCallWrapper<F, C> {

    private CompletableFuture<F> future;
    private Call<C> call;

    public FutureCallWrapper(CompletableFuture<F> future, Call<C> call) {
        this.future = future;
        this.call = call;
    }

    public CompletableFuture<F> getFuture() {
        return future;
    }

    public Call<C> getCall() {
        return call;
    }

    public void cancel() {
        if (future != null)
            future.cancel(false);

        if (call != null)
            call.cancel();
    }
}
