package com.bokugan.twitter_timeline.twitter;

import com.bokugan.twitter_timeline.BuildConfig;
import com.bokugan.twitter_timeline.twitter.utils.FutureCallWrapper;
import com.bokugan.twitter_timeline.twitter.utils.TweetsParameterRequestBuilder;

import java.util.List;

import java9.util.concurrent.CompletableFuture;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

// TODO. Use Dagger.
public final class TwitterAPI implements ITwitterAPI {
    private static final String API_KEY = "Basic " + BuildConfig.TWITTER_API_KEY;
    private static final String API_BASE_URL = "https://api.twitter.com/";
    private static final String BEARER_TOKEN_PREFIX = "Bearer ";
    // TODO. Refactor.
    private static final RequestBody OAUTH_REQUEST_BODY =
            RequestBody.create(null, "grant_type=client_credentials");

    private static final Object SYNC_OBJ = new Object();
    private static volatile ITwitterAPI instance;

    private final ITwitterRetrofit rest;
    private volatile String bearerToken;

    private TwitterAPI() {
        rest = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ITwitterRetrofit.class);
    }

    public static ITwitterAPI getInstance() {
        ITwitterAPI result = instance;
        if (result == null) {
            synchronized (SYNC_OBJ) {
                result = instance;
                if (result == null)
                    instance = result = new TwitterAPI();
            }
        }
        return result;
    }

    private CompletableFuture<String> getBearerTokenFuture() {
        String currentBearerToken = bearerToken;
        if (currentBearerToken != null)
            return CompletableFuture.completedFuture(currentBearerToken);

        return requestNewBearerTokenFuture().thenApply((newToken) -> {
            bearerToken = newToken;
            return newToken;
        });
    }

    private CompletableFuture<String> requestNewBearerTokenFuture() {
        FutureCallWrapper<String, OAuthResponse> futureCallWrapper = lastOAuthFutureCall;
        if (futureCallWrapper != null)
            futureCallWrapper.cancel();

        CompletableFuture<String> future = new CompletableFuture<>();
        Call<OAuthResponse> call = rest.auth(API_KEY, OAUTH_REQUEST_BODY);
        lastOAuthFutureCall = new FutureCallWrapper<>(future, call);

        call.enqueue(new Callback<OAuthResponse>() {
            @Override
            public void onResponse(Call<OAuthResponse> call, Response<OAuthResponse> response) {
                OAuthResponse oauthResp = response.body();
                future.complete(
                        response.isSuccessful() && oauthResp != null
                                ? oauthResp.getAccessToken()
                                : null);
            }

            @Override
            public void onFailure(Call<OAuthResponse> call, Throwable t) {
                future.complete(null);
            }
        });

        return future;
    }

    private FutureCallWrapper<String, OAuthResponse> lastOAuthFutureCall;

    @Override
    public CompletableFuture<FutureCallWrapper<List<Tweet>, List<Tweet>>> tweetsFuture(
            TweetsParameterRequestBuilder builder) {

        return getBearerTokenFuture()
                .thenApply((bearerToken) -> requestTweetsFuture(bearerToken, builder));
    }

    private FutureCallWrapper<List<Tweet>, List<Tweet>> requestTweetsFuture(
            String bearerToken,
            TweetsParameterRequestBuilder builder) {

        CompletableFuture<List<Tweet>> future = new CompletableFuture<>();
        Call<List<Tweet>> call = rest.tweets(
                BEARER_TOKEN_PREFIX + bearerToken,
                builder.getRequestParameters());

        call.enqueue(new Callback<List<Tweet>>() {
            @Override
            public void onResponse(Call<List<Tweet>> call, Response<List<Tweet>> response) {
                List<Tweet> tweets = response.body();
                future.complete(
                        response.isSuccessful() && tweets != null
                                ? tweets
                                : null);
            }

            @Override
            public void onFailure(Call<List<Tweet>> call, Throwable t) {
                future.complete(null);
            }
        });

        return new FutureCallWrapper<>(future, call);
    }
}
