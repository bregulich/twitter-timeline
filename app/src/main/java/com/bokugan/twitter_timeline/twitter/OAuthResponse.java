package com.bokugan.twitter_timeline.twitter;

import com.google.gson.annotations.SerializedName;

class OAuthResponse {
    @SerializedName("access_token")
    private String accessToken;

    public String getAccessToken() {
        return accessToken;
    }
}
