package com.bokugan.twitter_timeline.twitter;

import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

interface ITwitterRetrofit {
    @POST("oauth2/token")
    @Headers("Content-Type: application/x-www-form-urlencoded;charset=UTF-8")
    Call<OAuthResponse> auth(
            @Header("Authorization") String basicToken,
            @Body RequestBody body);

    @GET("1.1/statuses/user_timeline.json")
    Call<List<Tweet>> tweets(
            @Header("Authorization") String bearerToken,
            @QueryMap Map<String, String> options);
}
