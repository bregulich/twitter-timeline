package com.bokugan.twitter_timeline.twitter;

import com.google.gson.annotations.SerializedName;

public class Tweet {
    @SerializedName("text")
    private String text;

    @SerializedName("id")
    private long id;

    public String getText() {
        return text;
    }

    public long getId() {
        return id;
    }
}
