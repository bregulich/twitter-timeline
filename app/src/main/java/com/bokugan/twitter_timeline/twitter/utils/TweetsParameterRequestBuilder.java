package com.bokugan.twitter_timeline.twitter.utils;

import java.util.HashMap;
import java.util.Map;

public class TweetsParameterRequestBuilder {
    private Map<String, String> options;

    private void setField(String key, String value) {
        Map<String, String> options = getRequestParameters();
        if (value == null)
            options.remove(key);
        else
            options.put(key, value);
    }

    public Map<String, String> getRequestParameters() {
        if (options == null)
            options = new HashMap<>();

        return options;
    }

    public TweetsParameterRequestBuilder setUserId(String screenName) {
        setField("screen_name", screenName);
        return this;
    }

    public TweetsParameterRequestBuilder setCount (long count) {
        setField("count", Long.toString(count));
        return this;
    }

    public TweetsParameterRequestBuilder setMaxId (long maxId) {
        setField("max_id", Long.toString(maxId));
        return this;
    }

    public TweetsParameterRequestBuilder setSinceId (long sinceId) {
        setField("since_id", Long.toString(sinceId));
        return this;
    }
}
