package com.bokugan.twitter_timeline.twitter;

import com.bokugan.twitter_timeline.twitter.utils.FutureCallWrapper;
import com.bokugan.twitter_timeline.twitter.utils.TweetsParameterRequestBuilder;

import java.util.List;

import java9.util.concurrent.CompletableFuture;

public interface ITwitterAPI {
    CompletableFuture<FutureCallWrapper<List<Tweet>, List<Tweet>>> tweetsFuture(
            TweetsParameterRequestBuilder builder);
}
