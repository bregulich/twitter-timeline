package com.bokugan.twitter_timeline.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.SearchView;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.view.View;

import com.bokugan.twitter_timeline.R;
import com.bokugan.twitter_timeline.adapter.TweetAdapter;
import com.bokugan.twitter_timeline.viewmodel.MainViewModel;

public class MainActivity extends AppCompatActivity {

    private SwipeRefreshLayout refresher;
    private AppCompatTextView status;
    private RecyclerView recycler;
    private SearchView search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findWidgets();

        MainViewModel vm = ViewModelProviders.of(this).get(MainViewModel.class);

        TweetAdapter adapter = new TweetAdapter();

        recycler.setAdapter(adapter);
        recycler.setLayoutManager(new LinearLayoutManager(this));

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                vm.setUserId(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        refresher.setOnRefreshListener(vm::refreshData);

        vm.getTweets().observe(this, adapter::submitList);
        vm.getDataState().observe(this, this::updateDataState);
        vm.getUserId().observe(this, userId -> search.setQuery(userId, false));
    }

    private void findWidgets() {
        recycler = findViewById(R.id.recycler_view);
        search = findViewById(R.id.search);
        status = findViewById(R.id.status);
        refresher = findViewById(R.id.refresher);
    }

    private void updateDataState(MainViewModel.DataState state) {
        refresher.setRefreshing(
                state == MainViewModel.DataState.LOADING);

        status.setVisibility(
                state == MainViewModel.DataState.NOTHING
                        ? View.VISIBLE
                        : View.GONE);

        recycler.setVisibility(
                state == MainViewModel.DataState.NOTHING
                        ? View.INVISIBLE
                        : View.VISIBLE);
    }
}