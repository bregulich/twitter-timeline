package com.bokugan.twitter_timeline.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bokugan.twitter_timeline.R;
import com.bokugan.twitter_timeline.twitter.Tweet;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

public class TweetAdapter extends PagedListAdapter<Tweet, TweetAdapter.TweetViewHolder> {

    public TweetAdapter() {
        super(DIFF_CALLBACK);
    }

    public static class TweetViewHolder extends RecyclerView.ViewHolder {
        private TextView textView;

        public TweetViewHolder(@NonNull CardView itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.text_view);
        }

        public void bindTo(Tweet tweet) {
            textView.setText(
                    tweet == null
                            ? "TODO. loading."
                            : tweet.getText());

        }
    }

    @NonNull
    @Override
    public TweetViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CardView v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tweet_card_view, parent, false);

        TweetViewHolder vh = new TweetViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull TweetViewHolder holder, int position) {
        holder.bindTo(getItem(position));
    }

    private static final DiffUtil.ItemCallback<Tweet> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<Tweet>() {
                @Override
                public boolean areItemsTheSame(Tweet oldConcert, Tweet newConcert) {
                    return oldConcert.getId() == newConcert.getId();
                }

                @Override
                public boolean areContentsTheSame(Tweet oldConcert,
                                                  Tweet newConcert) {
                    return oldConcert.equals(newConcert);
                }
            };
}
