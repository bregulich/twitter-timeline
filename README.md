# twitter-timeline
![](https://media.giphy.com/media/1rOV0UTHqFQ31EbnTy/giphy.gif)

### Features
    - "lazy" timeline loading
    - swipe-to-refresh
    - AndroidX/Jetpack
    
### Requirements
    - Android studio 3.2+
    - Android 9 (API level 28)
    - twitter token

### Installation
Make sure you have a valid [token](https://developer.twitter.com/en/docs/basics/authentication/overview/application-only) for application-only requests. Then [add your token to gradle.properties file](https://medium.com/code-better/hiding-api-keys-from-your-android-repository-b23f5598b906). Here's the example of what it should look like inside:

    TWITTER_TIMELINE_VIEWER_TWITTER_API_KEY="your_token"

As of now you're free to run the app.